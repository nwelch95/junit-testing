package com.roi.museum;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Portfolio {

	private Map<Stock, Integer> holdings = null;

	public Portfolio() {
		holdings = new HashMap<Stock, Integer>();
	}

	public int getNumberOfHoldings() {
		return holdings.size();
	}

	public void addStock(Stock stock, int quantity) {
		holdings.put(stock, quantity);
	}

	public double getValue() {
		double value = 0;
		Set<Stock> stocks = holdings.keySet();
		for (Stock s : stocks) {
			int quantity = holdings.get(s);
			value += (quantity * s.getPrice());
		}
		return value;
	}
}
