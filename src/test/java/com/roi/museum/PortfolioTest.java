package com.roi.museum;

import static org.junit.Assert.*;

import org.junit.Test;

public class PortfolioTest {

	@Test
	public void addAStock() {
		Stock stock = new Stock(0.0);
		Portfolio portfolio = new Portfolio();
		int initialNumberOfStocks = portfolio.getNumberOfHoldings();

		portfolio.addStock(stock, 10);

		assertEquals(initialNumberOfStocks + 1, portfolio.getNumberOfHoldings());
	}

	@Test
	public void checkPortfolioValue() {
		Portfolio portfolio = new Portfolio();
		Stock stock1 = new Stock(50.0);
		Stock stock2 = new Stock(100.0);
		portfolio.addStock(stock1, 10);
		portfolio.addStock(stock2, 10);

		double totalValue = portfolio.getValue();

		assertEquals(1500, totalValue, 0.0001);
	}

	@Test
	public void checkInitialValue() {
		Portfolio portfolio = new Portfolio();

		double initialValue = portfolio.getValue();

		assertEquals(0, initialValue, 0.0001);
	}
}
